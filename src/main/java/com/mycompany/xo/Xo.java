/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.xo;

/**
 *
 * @author MYASUS
 */
import java.util.Scanner;
import java.util.Random;

public class Xo {
    public static boolean full(char[][] board) {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (board[row][col] == '_') {
                    return false;
                }
            }
        }
        return true;
    }

    public static void printBoard(char[][] board) {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.print(board[row][col] + " ");
            }
            System.out.println();
        }
    }

    public static char checkWinner(char[][] board) {

        for (int i = 0; i < 3; i++) {
            if (board[i][0] != '_' && board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
                return board[i][0];
            }
        }

        for (int i = 0; i < 3; i++) {
            if (board[0][i] != '_' && board[0][i] == board[1][i] && board[1][i] == board[2][i]) {
                return board[0][i];
            }
        }

        if (board[0][0] != '_' && board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
            return board[0][0];
        }

        if (board[0][2] != '_' && board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
            return board[0][2];
        }

        return 'w';
    }

    public static void main(String[] arg) {

        char[][] board = new char[3][3];
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome To OX Game");
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = '_';
                System.out.print(board[row][col] + " ");
            }
            System.out.println("");
        }

        System.out.println("Please input 1 to start");

        int game = scanner.nextInt();
        if (game == 1) {
            int ran = random.nextInt(2);
            System.out.println("");
            System.out.println("");
            System.out.println("");

            if (ran == 0) {
                int turn = 0;
                while (true) {
                    if (turn == 0) {
                        System.out.println("Turn X");
                        printBoard(board);
                        System.out.println("Please input (row,col)");
                        int rowx = scanner.nextInt() - 1;
                        int colx = scanner.nextInt() - 1;
                        if (rowx < 0 || rowx >= 3 || colx < 0 || colx >= 3 || board[rowx][colx] != '_') {
                            System.out.println("");
                            System.out.println("");
                            System.out.println("");
                            System.out.println("Sai ma mai");
                            char winner = checkWinner(board);
                            if (winner != 'w') {
                                System.out.println(winner + " wins!");
                                break;
                            } else if (full(board)) {
                                System.out.println("Drawn!");
                                break;
                            } else {
                                turn = 0;
                            }
                        } else {
                            board[rowx][colx] = 'X';
                            printBoard(board);
                            System.out.println("");
                            System.out.println("");
                            System.out.println("");
                            char winner = checkWinner(board);
                            if (winner != 'w') {
                                System.out.println(winner + " wins!");
                                break;
                            } else if (full(board)) {
                                System.out.println("Drawn!");
                                break;
                            } else {
                                turn = 1;
                            }
                        }
                    } else {
                        System.out.println("Turn O");
                        printBoard(board);
                        System.out.println("Please input (row,col)");
                        int rowo = scanner.nextInt() - 1;
                        int colo = scanner.nextInt() - 1;
                        if (rowo < 0 || rowo >= 3 || colo < 0 || colo >= 3 || board[rowo][colo] != '_') {
                            System.out.println("");
                            System.out.println("");
                            System.out.println("");
                            System.out.println("Sai ma mai");
                            char winner = checkWinner(board);
                            if (winner != 'w') {
                                System.out.println(winner + " wins!");
                                break;
                            } else if (full(board)) {
                                System.out.println("Drawn!");
                                break;
                            } else {
                                turn = 1;
                            }
                        } else {
                            board[rowo][colo] = 'O';
                            printBoard(board);
                            System.out.println("");
                            System.out.println("");
                            System.out.println("");
                            char winner = checkWinner(board);
                            if (winner != 'w') {
                                System.out.println(winner + " wins!");
                                break;
                            } else if (full(board)) {
                                System.out.println("Drawn!");
                                break;
                            } else {
                                turn = 0;
                            }
                        }
                    }
                }
            } else {
                int turn = 0;
                while (true) {
                    if (turn == 0) {
                        System.out.println("Turn O");
                        printBoard(board);
                        System.out.println("Please input (row,col)");
                        int rowo = scanner.nextInt() - 1;
                        int colo = scanner.nextInt() - 1;
                        if (rowo < 0 || rowo >= 3 || colo < 0 || colo >= 3 || board[rowo][colo] != '_') {
                            System.out.println("");
                            System.out.println("");
                            System.out.println("");
                            System.out.println("Sai ma mai");
                            char winner = checkWinner(board);
                            if (winner != 'w') {
                                System.out.println(winner + " wins!");
                                break;
                            } else if (full(board)) {
                                System.out.println("Drawn!");
                                break;
                            } else {
                                turn = 0;
                            }
                        } else {
                            board[rowo][colo] = 'O';
                            printBoard(board);
                            System.out.println("");
                            System.out.println("");
                            System.out.println("");
                            char winner = checkWinner(board);
                            if (winner != 'w') {
                                System.out.println(winner + " wins!");
                                break;
                            } else if (full(board)) {
                                System.out.println("Drawn!");
                                break;
                            } else {
                                turn = 1;
                            }
                        }
                    } else {
                        System.out.println("Turn X");
                        printBoard(board);
                        System.out.println("Please input (row,col)");
                        int rowx = scanner.nextInt() - 1;
                        int colx = scanner.nextInt() - 1;
                        if (rowx < 0 || rowx >= 3 || colx < 0 || colx >= 3 || board[rowx][colx] != '_') {
                            System.out.println("");
                            System.out.println("");
                            System.out.println("");
                            System.out.println("Sai ma mai");
                            char winner = checkWinner(board);
                            if (winner != 'w') {
                                System.out.println(winner + " wins!");
                                break;
                            } else if (full(board)) {
                                System.out.println("Drawn!");
                                break;
                            } else {
                                turn = 0;
                            }
                        } else {
                            board[rowx][colx] = 'X';
                            printBoard(board);
                            System.out.println("");
                            System.out.println("");
                            System.out.println("");
                            char winner = checkWinner(board);
                            if (winner != 'w') {
                                System.out.println(winner + " wins!");
                                break;
                            } else if (full(board)) {
                                System.out.println("Drawn!");
                                break;
                            } else {
                                turn = 1;
                            }
                        }
                    }
                }
            }
        } else {
            System.out.println("Quit");
        }
    }
}
